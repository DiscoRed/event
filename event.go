package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/bwmarrin/discordgo"
	_ "github.com/mattn/go-sqlite3"
)

// Constants
const eventMessage = `Post will be updated with necessary details (check pinned):
**Who**: %s
**What**: %s
**Where**: %s
**When**: %s
**Why**: %s
**How**: %s`

const helpString = `!event - Displays the event
!event new - Creates a new event
!event <section> <text> - Marks the section with the given text (<section> can be "what", "where", "when", "why", or "how")
!event [confirm|confirmed] - Marks you as confirmed
!event interested - Marks you as interested
!event withdraw - Clears you from the event's "who" section
!event notify <message> - Notifies everyone in the "who" section with <message>
!event archive - Archived the event
!event unarchive - Unarchives the event
!event delete - Deletes the channel's event`

// Global Variables
var l *log.Logger
var db *sql.DB

func init() {
	err := os.Chdir("modules/event")
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile("moduleEvent.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	l = log.New(file, "", log.Ldate|log.Ltime|log.Llongfile)

	db, err = sql.Open("sqlite3", "events.db")
	if err != nil {
		l.Fatalln(err)
	}

	const createEvents = `CREATE TABLE events (
		channelID text,
		what text,
		"where" text,
		"when" text,
		why text,
		how text,
		archived boolean
	);`
	_, err = db.Exec(createEvents)
	if err != nil {
		l.Println(err)
	}

	const createWho = `CREATE TABLE who (
		channelID text,
		id text,
		name text,
		status text
	);`
	_, err = db.Exec(createWho)
	if err != nil {
		l.Println(err)
	}

	const createMessages = `CREATE TABLE messages (
		channelID text,
		messageID text
	);`
	_, err = db.Exec(createMessages)
	if err != nil {
		l.Println(err)
	}
}

// SQL functions
func eventExists(channelID string) (exists bool) {
	rows, err := db.Query(`SELECT EXISTS(SELECT 1 FROM events WHERE channelID = ?)`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()
	rows.Next()

	err = rows.Scan(&exists)
	if err != nil {
		l.Fatalln(err)
	}
	return
}

func addEvent(channelID string) {
	_, err := db.Exec(`INSERT INTO events (channelID, what, "where", "when", why, how) VALUES (?, ?, ?, ?, ?, ?)`, channelID, "", "", "", "", "", false)
	if err != nil {
		l.Fatalln(err)
	}
}

func updateEvent(channelID string, col string, val string) {
	var err error
	switch col {
	case "what":
		_, err = db.Exec(`UPDATE events SET what = ? WHERE channelID = ?`, val, channelID)
	case "where":
		_, err = db.Exec(`UPDATE events SET "where" = ? WHERE channelID = ?`, val, channelID)
	case "when":
		_, err = db.Exec(`UPDATE events SET "when" = ? WHERE channelID = ?`, val, channelID)
	case "why":
		_, err = db.Exec(`UPDATE events SET why = ? WHERE channelID = ?`, val, channelID)
	case "how":
		_, err = db.Exec(`UPDATE events SET how = ? WHERE channelID = ?`, val, channelID)
	}
	if err != nil {
		l.Fatalln(err)
	}
}

func deleteEvent(channelID string) {
	_, err := db.Exec(`DELETE FROM events WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
}

func archiveEvent(channelID string) {
	_, err := db.Exec(`UPDATE events SET archived = TRUE WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
}

func unarchiveEvent(channelID string) {
	_, err := db.Exec(`UPDATE events SET archived = FALSE WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
}

func eventArchived(channelID string) (archived bool) {
	rows, err := db.Query(`SELECT archived FROM events WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()

	rows.Next()
	rows.Scan(&archived)
	return
}

func userExists(channelID string, id string) (exists bool) {
	rows, err := db.Query(`SELECT EXISTS(SELECT 1 FROM who WHERE channelID = ? and id = ?)`, channelID, id)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()
	rows.Next()

	err = rows.Scan(&exists)
	if err != nil {
		l.Fatalln(err)
	}
	return
}

func addUser(channelID string, id string, name string, status string) {
	_, err := db.Exec(`INSERT INTO who (channelID, id, "name", status) VALUES (?, ?, ?, ?)`, channelID, id, name, status)
	if err != nil {
		l.Fatalln(err)
	}
}

func updateUser(channelID string, id string, name string, status string) {
	_, err := db.Exec(`UPDATE who SET "name" = ?, status = ? WHERE channelID = ? AND id = ?`, name, status, channelID, id)
	if err != nil {
		l.Fatalln(err)
	}
}

func deleteUser(channelID string, id string) {
	_, err := db.Exec(`DELETE FROM who WHERE channelID = ? AND id = ?`, channelID, id)
	if err != nil {
		l.Fatalln(err)
	}
}

func deleteAllUsers(channelID string) {
	_, err := db.Exec(`DELETE FROM who WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
}

func getUserIDs(channelID string) (userIDs []string) {
	rows, err := db.Query(`SELECT id FROM who WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id string
		rows.Scan(&id)
		userIDs = append(userIDs, id)
	}
	return
}

func getMessages(channelID string) (messages []string) {
	rows, err := db.Query(`SELECT messageID FROM messages WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()

	for rows.Next() {
		var messageID string
		err := rows.Scan(&messageID)
		if err != nil {
			l.Fatalln(err)
		}

		messages = append(messages, messageID)
	}
	return
}

func deleteMessages(channelID string) {
	_, err := db.Exec(`DELETE FROM messages WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
}

// Helper functions
func editWho(status string, names []string) (who string) {
	if len(names) == 0 {
		return
	}

	sort.Strings(names)
	for _, v := range names {
		who += v + ", "
	}
	who = who[:len(who)-2]
	who += fmt.Sprintf(" (%s) ", status)
	return
}

func getEvent(channelID string) (who, what, where, when, why, how string) {
	rows, err := db.Query(`SELECT what, "where", "when", why, how FROM events WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()
	rows.Next()

	err = rows.Scan(&what, &where, &when, &why, &how)
	if err != nil {
		l.Fatalln(err)
	}
	if what == "" {
		what = "???"
	}
	if where == "" {
		where = "???"
	}
	if when == "" {
		when = "???"
	}
	if why == "" {
		why = "???"
	}
	if how == "" {
		how = "???"
	}

	rowsWho, err := db.Query(`SELECT "name", status FROM who WHERE channelID = ?`, channelID)
	if err != nil {
		l.Fatalln(err)
	}
	defer rowsWho.Close()

	statuses := make(map[string][]string)
	for rowsWho.Next() {
		var name string
		var status string
		err := rowsWho.Scan(&name, &status)
		if err != nil {
			l.Fatalln(err)
		}
		statuses[status] = append(statuses[status], name)
	}

	who += editWho("confirmed", statuses["confirmed"])
	who += editWho("interested", statuses["interested"])

	for k, v := range statuses {
		if k != "confirmed" && k != "interested" {
			who += editWho(k, v)
		}
	}

	if who == "" {
		who = "???"
	}

	return
}

func addEventMessage(channelID string, messageID string) {
	_, err := db.Exec(`INSERT INTO messages (channelID, messageID) VALUES (?, ?)`, channelID, messageID)
	if err != nil {
		l.Fatalln(err)
	}
}

func getEventMessage(who, what, where, when, why, how string) string {
	return fmt.Sprintf(eventMessage, who, what, where, when, why, how)
}

func editAll(dg *discordgo.Session, channelID string) {
	msg := getEventMessage(getEvent(channelID))
	messages := getMessages(channelID)
	for _, v := range messages {
		dg.ChannelMessageEdit(channelID, v, msg)
	}
}

func modifyEvent(dg *discordgo.Session, channelID string, args []string) {
	updateEvent(channelID, args[0], strings.Join(args[1:], " "))
	editAll(dg, channelID)
}

func markUser(dg *discordgo.Session, channelID string, user *discordgo.User, status string) {
	if userExists(channelID, user.ID) {
		l.Println("exists")
		updateUser(channelID, user.ID, user.Username, status)
	} else {
		l.Println("don't exist")
		addUser(channelID, user.ID, user.Username, status)
	}

	editAll(dg, channelID)
}

func removeMessages(dg *discordgo.Session, channelID string) {
	messages := getMessages(channelID)
	for _, v := range messages {
		dg.ChannelMessageDelete(channelID, v)
	}
}

func notify(dg *discordgo.Session, channelID string, sender *discordgo.User, note string) {
	userIDs := getUserIDs(channelID)
	var message string
	for _, name := range userIDs {
		message += fmt.Sprintf("<@%s> ", name)
	}
	message += fmt.Sprintf("\n%s says:\n%s", sender.Username, note)
	dg.ChannelMessageSend(channelID, message)
}

func handleMessage(dg *discordgo.Session, m discordgo.Message) {
	l.Println(m.Content)

	channelID := m.ChannelID
	args := strings.Split(m.Content, " ")[1:]

	exists := eventExists(channelID)
	l.Println(exists)

	if len(args) > 0 && args[0] == "help" { // Handle help first
		directMessage(dg, m.Author, helpString)
		return
	}

	if !exists && (len(args) == 0 || args[0] != "new") { // No event, and user does not create one
		dg.ChannelMessageSend(channelID, `No event found. Please create one with "!event new"`)
		return
	} else if !exists && args[0] == "new" { // No event and a user creates one
		addEvent(channelID)
		newEvent, err := dg.ChannelMessageSend(channelID, getEventMessage(getEvent(channelID)))
		if err != nil {
			l.Fatalln(err)
		}

		addEventMessage(channelID, newEvent.ID)
		dg.ChannelMessagePin(channelID, newEvent.ID)
		return
	}
	if len(args) == 0 { // !event
		msg := getEventMessage(getEvent(channelID))
		l.Println(msg)

		eventMsg, err := dg.ChannelMessageSend(channelID, msg)
		if err != nil {
			l.Fatalln(err)
		}

		addEventMessage(channelID, eventMsg.ID)
		return
	}

	// Prioritized above archive checking
	if args[0] == "new" {
		dg.ChannelMessageSend(channelID, `Event already exists. Checked pins or enter "!event" to view it.`)
		return
	} else if args[0] == "unarchive" {
		if eventArchived(channelID) {
			unarchiveEvent(channelID)
			dg.ChannelMessageSend(channelID, `Event is now unarchived.`)
		} else {
			dg.ChannelMessageSend(channelID, `Event not archived.`)
		}
		return
	} else if args[0] == "delete" {
		deleteEvent(channelID)
		deleteAllUsers(channelID)
		removeMessages(dg, channelID)
		deleteMessages(channelID)
		return
	}

	if eventArchived(channelID) {
		dg.ChannelMessageSend(channelID, `Event is archived, and no updates can be made. Enter "!event unarchive" to unarchive the event.`)
		return
	}

	l.Println(args[0])
	switch args[0] {
	case "what", "where", "when", "why", "how":
		l.Println("Modifying")
		modifyEvent(dg, channelID, args)
	case "confirmed", "interested":
		l.Println(args[0])
		markUser(dg, channelID, m.Author, args[0])
	case "confirm":
		l.Println(args[0])
		markUser(dg, channelID, m.Author, "confirmed")
	case "withdraw":
		deleteUser(channelID, m.Author.ID)
		editAll(dg, channelID)
	case "archive":
		archiveEvent(channelID)
		dg.ChannelMessageSend(channelID, `Event is now archived. Enter "!event unarchive" to unarchive it.`)
	case "notify":
		notify(dg, channelID, m.Author, strings.Join(args[1:], " "))
	default:
		dg.ChannelMessageSend(channelID, `Unknown command. Enter "!event help" to see valid commands.`)
	}
}

func directMessage(dg *discordgo.Session, user *discordgo.User, message string) {
	c, err := dg.UserChannelCreate(user.ID)
	if err != nil {
		l.Fatalln(err)
	}

	dg.ChannelMessageSend(c.ID, message)
}

func cmdEvent(args []string) {
	token := args[0]
	message := args[1]

	var m discordgo.Message
	err := json.Unmarshal([]byte(message), &m)
	if err != nil {
		l.Println(err)
		return
	}

	// Create a new Discord session using bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		l.Println("Error creating Discord session:", err)
		return
	}
	l.Println("Discord session created")

	// Open the web socket and begin listening.
	err = dg.Open()
	if err != nil {
		l.Println("Error opening Discord session:", err)
	}
	l.Println("New session opened")

	// Close down after completion
	defer func() {
		dg.Close()
		l.Println("New session closed")
	}()

	handleMessage(dg, m)
}

// Entry point
func main() {
	l.Println("Command is event")
	args := os.Args[1:]
	cmdEvent(args)
}
